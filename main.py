import datetime
import logging
import socket
import os
import psq
import csv
#import tasks

from flask import current_app, Flask, request, jsonify
from werkzeug import secure_filename
from google.cloud import datastore, pubsub

app = Flask(__name__)

app.config['UPLOAD_FOLDER'] = 'uploads/'
app.config['ALLOWED_EXTENSIONS'] = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif', 'csv'])

if not os.path.exists(app.config['UPLOAD_FOLDER']):
	os.makedirs(app.config['UPLOAD_FOLDER'])


PROJECT_ID = 'deploy2-163408'
ds = datastore.Client(PROJECT_ID)
ps = pubsub.Client(PROJECT_ID)
q = psq.Queue(ps, storage=psq.DatastoreStorage(ds))


def is_ipv6(addr):
	try:
		socket.inet_pton(socket.AF_INET6, addr)
		return True
	except socket.error:
		return False

def allowed_file(filename):
	return '.' in filename and \
	         filename.rsplit('.', 1)[1] in app.config['ALLOWED_EXTENSIONS']


### psq task ###

def process_data():
	#ds = datastore.Client(PROJECT_ID)
	dir_path = os.listdir(app.config['UPLOAD_FOLDER'])
	for file in dir_path:
		if file.endswith('.csv'):
			with open(app.config['UPLOAD_FOLDER'] + file, 'r') as f:
				for line in csv.reader(f):
					if not ''.join(line).strip():
						empty_lines += 1
						continue
					else:
						get_data = int(line)

						entity = datastore.Entity(key=ds.key('spectro_read'))
						entity.update({
							'Timestamp': datetime.datetime.utcnow(),
							'Data': get_data
							})

						ds.put(entity)

#with app.app_context():
#	task_queue = process_data()

### flask route ###
@app.route('/', methods=['GET', 'POST'])
def index():
	if request.method == 'POST':
		file = request.files['file']
		if file and allowed_file(file.filename):
			filename = secure_filename(file.filename)
			file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))

			#if 'file' in request.files:
		q.enqueue(process_data)

	return 'Processing...'


@app.route('/show', methods=['GET'])
def get_data():
	#ds = datastore.Client()
	entity = datastore.Entity(key=ds.key('spectro_read'))
	query = ds.query(kind='spectro_read', order=('-Timestamp',))
	for entity in query.fetch(limit=100):
		#return entity.obj
		return jsonify(entity)
	#return jsonify() ### get data from datastore ###


@app.errorhandler(500)
def server_error(e):
	logging.exception('An error occured during a request.')
	return """
	An internal error occured: <pre>{}</pre>
	See logs for full stacktrace.
	""".format(e), 500


if __name__ == '__main__':
	app.run(host='127.0.0.1', port=8080, debug=True)
